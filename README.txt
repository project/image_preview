CONTENTS OF THIS FILE
---------------------

 * Requirements
 * Installation


REQUIREMENTS
------------
Install and enable:
 * Image (drupal core)
 * Library API: http://drupal.org/project/libraries


INSTALLATION
------------
 * Download hoverintent from
   http://cherne.net/brian/resources/jquery.hoverIntent.html
 * Put it in: sites/all/libraries
 * Name of script: jquery.hoverIntent.minified.js
